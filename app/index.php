<?php

/**
 * Функция из задания, возращает данные из БД
 * @param $id
 * @return array
 */
function getAdRecord($id)
{
    return [
        'id'       => $id,
        'name'     => 'AdName_FromMySQL',
        'text'     => 'AdText_FromMySQL',
        'keywords' => 'Some Keywords',
        'price'    => 10,
    ];
}

/**
 * Функция из задания, возращает данные из кеша
 * @param $id
 * @return string
 */
function get_deamon_ad_info($id)
{
    return "{$id}\t235678\t12348\tAdName_FromDaemon\tAdText_FromDaemon\t11";
}

/**
 * Курс доллара в центах полученный и БД
 * @var array
 */
$courses = [
    'rur' => 6534,
    'eur' => 88
];



/**
 * Подключение библиотек
 */
require 'lib/SimpleCourse.php';
require 'lib/SimpleTask.php';
require 'lib/SimpleLogger.php';

/**
 * Инициализация класса конвертации доллара
 * @var SimpleCourse
 */
$courses = new SimpleCourse($courses);

/**
 * Инициализация класса тестового задания
 * @var SimpleTask
 */
$simple = new SimpleTask();

$data = $simple->getData((int) $_GET['id'], $_GET['from']);

?>


<?php
/**
 * Рендеринг, если данные полученны
 */
if ($data) {
    ?>
    <h1>
        <?= $data->offsetGet('name') ?>
    </h1>
    <p>
        <?= $data->offsetGet('text') ?>
    </p>
    <p>
        Стоимость: <?= $courses->convert('rur', $data->offsetGet('price')) ?> руб
    </p>
<?php
    }
    ?>
