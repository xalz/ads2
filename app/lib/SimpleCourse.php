<?php

/**
 * Конвертация доллара в другую валюту
 * Class SimpleCourse
 */
class SimpleCourse
{

    protected $courses;

    /**
     * SimpleCourse constructor.
     * @param array $courses
     */
    public function __construct(array $courses)
    {
        $this->courses = $courses;
    }

    /**
     * Конвертация долларов в другую валюту
     * @param string $currency
     * @param float $price
     * @return float
     */
    public function convert(string $currency, float $price): float
    {
        return $this->getCents($price) * $this->getCourse($currency) / 100;
    }

    /**
     * Получение курса доллара
     * @param string $currency
     * @return float
     */
    protected function getCourse(string $currency): float
    {
        return $this->courses[$currency] / 100;
    }

    /**
     * Получение центов
     * @param float $price
     * @return int
     */
    protected function getCents(float $price): int
    {
        return $price * 100;
    }
}