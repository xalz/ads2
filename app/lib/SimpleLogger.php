<?php

/**
 * Класс подготовки строки логгирования
 * Class SimpleLogger
 */
class SimpleLogger
{

    /**
     * Запись в лог
     * @param int $id
     * @param string $method
     */
    public static function setLog(int $id, string $method): void
    {
        $message = self::getMessage($id, $method);

        /**
         * Тут пишем $message в лог.
         */
        //$logger = new FileLogger('/path/to/file');
        //$logger->log($message);
    }

    /**
     * Подготовка строки лога
     * @param int $id
     * @param string $method
     * @return string
     */
    protected static function getMessage(int $id, string $method): string
    {
        return '[' . date('Y-m-d H:i:s') . '] ' . "{$method}({$id})";
    }

}