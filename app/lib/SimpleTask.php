<?php

/**
 * Класс тестового задания
 * Class SimpleTask
 */
class SimpleTask
{

    /**
     * Получение данных
     * @param int|null $id
     * @param string|null $repository
     * @return ArrayObject|null
     */
    public function getData(int $id = null, string $repository = null): ?\ArrayObject
    {
        if (!$id) {
            return null;
        }

        switch (strtolower($repository)) {
            case 'mysql':
                return $this->getFromDb($id);
            case 'daemon':
                return $this->getFromCache($id);
        }

        return null;
    }

    /**
     * Получение данных из БД
     * @param int $id
     * @return ArrayObject|null
     */
    protected function getFromDb(int $id): ?\ArrayObject
    {
        SimpleLogger::setLog($id, __METHOD__);

        $data = getAdRecord($id);

        return new \ArrayObject($data);
    }

    /**
     * Получение данных от демона
     * @param int $id
     * @return ArrayObject|null
     */
    protected function getFromCache(int $id): ?\ArrayObject
    {
        SimpleLogger::setLog($id, __METHOD__);

        if ($data = $this->parseCacheData(get_deamon_ad_info($id))) {
            return new \ArrayObject($data);
        }

        return null;
    }

    /**
     * Декомпозиция данных полученных от демона
     * @param string $data
     * @return array|null
     */
    protected function parseCacheData(string $data): ?array
    {
        /**
         * Паттерн регулярного выражения для декомпозиции строки данных
         * Учтена плавающая точка в цене
         * @var string
         */
        $pattern = '#(.*?)\t+(\d+)\t+(\d+)\t+(.*?)\t+(.*?)\t+(\d+(?:[\.,]\d+)?)#';

        if (!preg_match($pattern, $data, $out)) {
            return null;
        }

        return [
            'id' => $out[1],
            'name' => $out[4],
            'text' => $out[5],
            'price' => $out[6]
        ];
    }

}